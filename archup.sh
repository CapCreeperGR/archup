#!/bin/bash

BACKTITLE="ArchUP"

HOSTNAME="archlinux"
declare -a USERS=()
TUI_EDITOR="Nano"
SHELL="Bash"
AUR="None"
DE_WM="None"
LOGIN_MANAGER="None"
LOCALE="en_US.UTF-8 UTF-8"

main_menu() {
  HEIGHT=20
  WIDTH=60
  CHOICE_HEIGHT=4
  TITLE="Main Menu"
  SUBTITLE="Choose an option"
  OPTIONS=(1 "Set Hostname ($HOSTNAME)"
           2 "Set Root Password ($(if [ -z "${ROOT_PASS}" ]; then echo -n "Not Set"; else echo -n "Set"; fi))"
           3 "Setup User Accounts ($(if [ ${#USERS[@]} -ne 1 ]; then echo -n "${#USERS[@]} Users"; else echo -n "1 User"; fi))"
           4 "Set Terminal Text Editor ($TUI_EDITOR)"
           5 "Set User Shell ($SHELL)"
           6 "Set AUR Helper ($AUR)"
           7 "Set Desktop Environment/Window Manager ($DE_WM)"
           8 "Set Login/Display Manager ($LOGIN_MANAGER)"
           9 "Set Locale ($LOCALE)"
           10 "Set Timezone ($(if [ -z "${TIMEZONE}" ]; then echo -n "Not Set"; else echo -n "$TIMEZONE"; fi))"
           11 "Partition Drives (Under Development)"
           12 "Start Installation")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  case $option in
  1) set_hostname;;
  2) set_root_password;;
  3) setup_users;;
  4) set_terminal_text_editor;;
  5) set_user_shell;;
  6) set_aur_helper;;
  7) set_de_wm;;
  8) set_login_manager;;
  9) set_locale;;
  10) set_timezone;;
  11) main_menu;; #setup_partitions;;
  12) start_installation;;
  *) clear; echo "Installation Cancelled"; exit 0;;
  esac
}

set_hostname() {
  HEIGHT=8
  WIDTH=60
  TITLE="Hostname"
  SUBTITLE="Set a hostname for your system\nThe hostname can only contain lowercase letters"
  HOSTNAME=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --inputbox "$SUBTITLE" $HEIGHT $WIDTH "$HOSTNAME" 2>&1 >/dev/tty)
  if [[ ! "$HOSTNAME" =~ ^[a-z]+$ ]]; then
    set_hostname
    return
  fi
  main_menu
}

set_root_password() {
  HEIGHT=8
  WIDTH=60
  TITLE="Root password"
  SUBTITLE="Set a root password for your system\nA password can only contain letters, numbers underscores and dashes"
  ROOT_PASS=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --insecure --passwordbox "$SUBTITLE" $HEIGHT $WIDTH 2>&1 >/dev/tty)
  if [[ ! "$ROOT_PASS" =~ ^[a-zA-Z0-9_-]+$ ]]; then
    set_root_password
    return
  fi
  main_menu
}

setup_users() {
  if [ ${#USERS[@]} -eq 0 ]; then
    edit_user "" "" "" "wheel,storage"
  else
    HEIGHT=15
    WIDTH=60
    CHOICE_HEIGHT=4
    TITLE="Setup Users"
    SUBTITLE="Select a user to edit/remove\nUse cancel to exit"
    declare -a OPTIONS=()
    for i in $(seq ${#USERS[@]}); do
      OPTIONS+=($i $(echo "${USERS[(($i - 1))]}" | cut -d":" -f1))
    done
    OPTIONS+=($((${#USERS[@]} + 1)) "Add User")
    option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
    if [ -z "$option" ]; then
      main_menu
    elif [[ "$option" -eq "$((${#USERS[@]} + 1))" ]]; then
      edit_user
    elif [ ! -z "$option" ]; then
      user="${USERS[(($option - 1))]}"
      edit_user "$option" "$(echo "$user" | cut -d":" -f1 | tr -d '\n')" "$(echo "$user" | cut -d":" -f2 | tr -d '\n')" "$(echo "$user" | cut -d":" -f3 | tr -d '\n')"
    fi
  fi
}

edit_user() {
  unset username
  unset pass
  unset groups
  HEIGHT=15
  WIDTH=60
  CHOICE_HEIGHT=4
  SUBTITLE="Choose an option"
  if [ -z "$4" ]; then GROUPS_LEN=0; else GROUPS_LEN=$(echo "$4" | sed "s/,/\n/g" | wc -l); fi
  OPTIONS=(1 "Set Username ($2)"
           2 "Set Password ($(if [ -z "$3" ]; then echo "Not Set"; else echo "Set"; fi))"
           3 "Set Groups ($GROUPS_LEN Groups)"
           4 "Save User")
  if [ -z "$1" ]; then
    TITLE="Create User"
  else
    OPTIONS+=(5 "Delete User")
    TITLE="Edit User"
  fi
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  case $option in
  1)
  until [[ "$username" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; do
    username=$(dialog --clear --backtitle "$BACKTITLE" --title "Set Username" --inputbox "A username can only contain letters and numbers" 8 60 "$2" 2>&1 >/dev/tty)
  done
  edit_user "$1" "$username" "$3" "$4";;
  2)
  until [[ "$pass" =~ ^[a-zA-Z0-9_-]+$ ]]; do
    pass=$(dialog --clear --backtitle "$BACKTITLE" --title "Set User Password" --insecure --passwordbox "A password can only contain letters, numbers underscores and dashes" 8 60 "$3" 2>&1 >/dev/tty)
  done
  edit_user "$1" "$2" "$pass" "$4";;
  3)
  until [[ "$groups" =~ ^[a-zA-Z0-9_,-]+$ ]]; do
    groups=$(dialog --clear --backtitle "$BACKTITLE" --title "Set User Groups" --inputbox "Seperate groups with commas" 8 60 "$4" 2>&1 >/dev/tty)
  done
  edit_user "$1" "$2" "$3" "$groups";;
  4)
  if [ -z "$2" ] || [ -z "$3" ]; then
    dialog --clear --backtitle "$BACKTITLE" --title "Could not create user account" --msgbox "Both the username and password have to be non-empty" 8 60 2>&1 >/dev/tty
    edit_user "$1" "$2" "$3" "$4"
  else
    if [ ! -z "$1" ]; then USERS[$(($1 - 1))]="$(echo "${2}:${3}:${4}" | tr -d '\n')"; else USERS+=("$(echo "${2}:${3}:${4}" | tr -d '\n')"); fi
    setup_users
  fi
  ;;
  5)
  if [ ! -z "$1" ]; then unset "USERS[$(($1 - 1))]"; fi
  if [ ${#USERS[@]} -eq 0 ]; then main_menu; else setup_users; fi
  ;;
  *) if [ -z "$1" ]; then main_menu; else setup_users; fi;;
  esac
}

set_terminal_text_editor() {
  HEIGHT=15
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="Terminal Text Editor"
  SUBTITLE="Choose an option\nThe terminal text editor is the program you will use to edit text files in linux through the command line"
  OPTIONS=(1 "Nano (Recommended)"
           2 "Vi"
           3 "Vim"
           4 "Neovim"
           5 "Emacs")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z "$option" ]; then
    TUI_EDITOR="${OPTIONS[$(($option*2-1))]}"
    TUI_EDITOR="$(echo -n "$TUI_EDITOR" | cut -d" " -f1)"
  fi
  main_menu
}

set_user_shell() {
  HEIGHT=15
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="User Shell"
  SUBTITLE="Choose an option\nA shell is a command line program that is used to run programs, scripts and interface with the linux system\nPOSIX-Compliant shells have higher script compatibility but their syntax can get very complicated at times\nNon-POSIX Compliant shells usually have simpler snytax at the cost of less script compatibility"
  OPTIONS=(1 "Bash (POSIX-Compliant, Recommended)"
           2 "Zsh (POSIX-Compliant)"
           3 "Fish (Non POSIX-Compliant)")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z "$option" ]; then
    SHELL="${OPTIONS[$(($option*2-1))]}"
    SHELL="$(echo -n "$SHELL" | cut -d" " -f1)"
  fi
  main_menu
}

set_login_manager() {
  HEIGHT=15
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="Login/Display Manager"
  SUBTITLE="Choose an option\nA Login or Display Manager is the program that is launched during startup and provides you with a GUI login interface\nSome Desktop Environments have better support for a specific Login Manager than others, but generally any will work"
  OPTIONS=(1 "None"
           2 "SDDM (Recommended for Plasma)"
           3 "GDM (Recommended for Gnome)"
           4 "LightDM")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z "$option" ]; then
    LOGIN_MANAGER="${OPTIONS[$(($option*2-1))]}"
    LOGIN_MANAGER="$(echo -n "$LOGIN_MANAGER" | cut -d" " -f1)"
  fi
  main_menu
}

set_aur_helper() {
  HEIGHT=15
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="AUR Helper"
  SUBTITLE="Choose an option\nThe AUR is short for the Arch User Repository. A large collection of user uploaded packages to the internet. An AUR helper is required to easily access and download these packages. Either AUR helper will function similarly"
  OPTIONS=(1 "None"
           2 "Yay"
           3 "Paru")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z "$option" ]; then
    AUR="${OPTIONS[$(($option*2-1))]}"
    AUR="$(echo -n "$AUR" | cut -d" " -f1)"
  fi
  main_menu
}

set_de_wm() {
  HEIGHT=20
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="Desktop Environment/Window Manager"
  SUBTITLE="Choose an option\nDesktop Environment (DEs) are fully fledged GUI desktops with an emphasis on ease of use and work out of the box\nWindow Managers (WMs) are barebones GUI window management systems that put emphasis on simplicity and require extensive configuration"
  OPTIONS=(1 "None"
           2 "(DE) Plasma (Recommended to beginners)"
           3 "(DE) Gnome (Recommended to beginners)"
           4 "(DE) Cinnamon (Recommended to beginners)"
           5 "(DE) MATE"
           6 "(DE) XFCE"
           6 "(DE) LXQT"
           7 "(DE) LXDE"
           8 "(WM) i3"
           9 "(WM) Bspwm"
           10 "(WM) Awesome"
           11 "(WM) Hyprland"
           12 "(WM) Sway")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)

  if [ ! -z "$option" ]; then
    DE_WM="${OPTIONS[$(($option*2-1))]}"
    DE_WM=$(echo -n $DE_WM | cut -d" " -f2)
  fi
  main_menu
}

set_locale() {
  HEIGHT=16
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="System Locale"
  SUBTITLE="Select a system locale\nLocales are files used by a lot of programs to render text correctly\nYou may think of this as selecting a language"
  OPTIONS=()
  i=1
  while read line; do
    OPTIONS+=($i "$line")
    ((i++))
  done < /usr/share/i18n/SUPPORTED
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z $option ]; then
    LOCALE="${OPTIONS[$(($option*2-1))]}"
  fi
  main_menu
}

set_timezone() {
  HEIGHT=16
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="System Timezone"
  SUBTITLE="Select your region"
  OPTIONS=()
  i=1
  while read line; do
    OPTIONS+=($i "${line##*/}")
    ((i++))
  done < <(find /usr/share/zoneinfo -mindepth 1 -maxdepth 1 -type d)
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  if [ ! -z $option ]; then
    REGION="${OPTIONS[$(($option*2-1))]}"
    SUBTITLE="Select your timezone"
    OPTIONS=()
    i=1
    while read line; do
      OPTIONS+=($i "${line##*/}")
      ((i++))
    done < <(find /usr/share/zoneinfo/"$REGION" -mindepth 1 -maxdepth 1 -type f)
    option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
    if [ ! -z $option ]; then
      TIMEZONE="$REGION/${OPTIONS[$(($option*2-1))]}"
    fi
  fi
  main_menu
}

setup_partitions() {
  HEIGHT=16
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="Partitioning"
  SUBTITLE="Choose an option\nA linux system is usually made up of 3-4 partitions\nThe EFI partition which contains the bootloader (On UEFI systems only)\nThe swap partition which is used as extra memory in case RAM runs out\nThe root partition which is where the system files are located\nThe home partition which is where user directories are stored (Optional)\nA seperate home partition makes reinstalling easier without loss of data"
  OPTIONS=(1 "Automatic with seperate home partition (Recommended)"
           2 "Automatic with combined root and home partition (Recommended for devices with less than 64GiB of storage)"
           3 "Manual")
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  case $option in
  1) auto_partition true;;
  2) auto_partition false;;
  3) ;;
  *) main_menu;;
  esac
}

auto_partition() {
  HEIGHT=16
  WIDTH=100
  CHOICE_HEIGHT=4
  TITLE="Partitioning"
  SUBTITLE="Select a storage device to install arch linux to\nWARNING: ALL DATA ON THE SELECTED DEVICE WILL BE DELTED"
  OPTIONS=()
  i=1
  for device in $(find /sys/block/ -type l); do
    OPTIONS+=($i "${device##*/}")
    ((i++))
  done
  option=$(dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --menu "$SUBTITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" 2>&1 >/dev/tty)
  sysblock="/sys/block/${OPTIONS[$(($option*2-1))]}"
  device="/dev/${OPTIONS[$(($option*2-1))]}"
  total_size=$(($(cat $sysblock/size) * 512 / 1024 / 1024))
  ram_size=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1024))
  declare -a part_size=()
  if [ -f /sys/firmware/efi/fw_platform_size ]; then
    part_size[0]=512
  fi
  if [ $ram_size -le 4096 ]; then
    part_size[1]=$(($ram_size * 2))
  elif [ $ram_size -gt 4096 ] && [ $ram_size -le 8192 ]; then
    part_size[1]=$(($ram_size))
  elif [ $ram_size -le 16384 ]; then
    part_size[1]=$(($ram_size / 2))
  else
    part_size[1]=8192
  fi
  if [ $1 == true ]; then
    remaining=$(( $total_size - (${part_size[0]} + ${part_size[1]}) ))
    part_size[2]=$(awk "BEGIN {print int(0.4*$remaining)}")
    if [ ${part_size[2]} -gt 65536 ]; then
      part_size[2]=65536
    fi
    part_size[3]=$(($remaining - ${part_size[2]}))
  else
    part_size[2]=$(( total_size - (${part_size[0]} + ${part_size[1]}) ))
  fi
  SUBTITLE="Partitions to be created\n"
  i=1
  part_use=("EFI Partition" "Swap Partition" "Home Partition" "Root Partition")
  part_type=("FAT32" "SWAP" "EXT4" "EXT4")
  for part in ${part_size[@]}; do
    if [ $part -lt 1024 ]; then
      SUBTITLE+="${device}${i} ${part} MiB (${part_use[(($i - 1))]}) [${part_type[(($i - 1))]}]\n"
    else
      SUBTITLE+="${device}${i} $(($part / 1024)) GiB (${part_use[(($i - 1))]}) [${part_type[(($i - 1))]}]\n"
    fi
    ((i++))
  done
  if ! dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --yesno "$SUBTITLE" $HEIGHT $WIDTH 2>&1 >/dev/tty; then
    main_menu
  fi
  PARTITIONS=()
}

start_installation() {
  if [ -z "$ROOT_PASS" ]; then
    dialog --clear --backtitle "$BACKTITLE" --title "Cannot procceed with installation" --msgbox "Root password cannot be empty" 6 60 2>&1 >/dev/tty
    main_menu
    return
  fi
  if [ -z "$TIMEZONE" ]; then
    dialog --clear --backtitle "$BACKTITLE" --title "Cannot procceed with installation" --msgbox "Timezone cannot be empty" 6 60 2>&1 >/dev/tty
    main_menu
    return
  fi
  if ! mountpoint -q -- /mnt/; then
    dialog --clear --backtitle "$BACKTITLE" --title "Cannot procceed with installation" --msgbox "No partition has been mounted to /mnt" 6 60 2>&1 >/dev/tty
    main_menu
    return
  fi
  HEIGHT=24
  WIDTH=60
  TITLE="Verify Installation"
  read -r -d '' SUBTITLE << EOM
Do the following settings seem correct?
Hostname: $HOSTNAME
$(if [ ${#USERS[@]} -eq 0 ]; then echo "No Users Created"; else
    declare -a USERNAMES=()
    for i in $(seq ${#USERS[@]}); do
      USERNAMES+=($(echo ${USERS[(($i - 1))]} | cut -d":" -f1))
    done
    echo "${#USERS[@]} Users Created (${USERNAMES[@]})"
fi)
Terminal Text Editor: $TUI_EDITOR
User Shell: $SHELL
AUR Helper: $AUR
Desktop Environment/Window Manager: $DE_WM
System Locale: $LOCALE
System Timezone: $TIMEZONE
Login/Display Manager: $LOGIN_MANAGER
EOM
  if ! dialog --clear --backtitle "$BACKTITLE" --title "$TITLE" --yesno "$SUBTITLE" $HEIGHT $WIDTH 2>&1 >/dev/tty; then
    main_menu
  fi
  if dialog --clear --backtitle "$BACKTITLE" --title "Save Config" --yesno "Would you like to save the current configuration to a file?\n[WARNING] This will store the root and user passwords in plain text! Do not share configuration files with others" $HEIGHT $WIDTH 2>&1 >/dev/tty; then
        mkdir -p /mnt/etc/archup/
        touch /mnt/etc/archup/"$HOSTNAME".config
        cat > /mnt/etc/archup/"$HOSTNAME".config << EOF
HOSTNAME=$HOSTNAME
ROOT_PASS=$ROOT_PASS
USERS=$USERS
TUI_EDITOR=$TUI_EDITOR
SHELL=$SHELL
AUR=$AUR
DE_WM=$DE_WM
LOCALE=$LOCALE
TIMEZONE=$TIMEZONE
LOGIN_MANAGER=$LOGIN_MANAGER
EOF
    dialog --clear --backtitle "$BACKTITLE" --title "Save Config" --msgbox "Configuration file saved to /etc/archup/${HOSTNAME}.config inside the new installation" 8 60 2>&1 >/dev/tty
  fi
  clear
  echo "Starting installation..."
  echo "Mounting partitions..."
  declare -a pkgs=("base" "linux" "linux-firmware")
  pkgs+=("${TUI_EDITOR,,}")
  pkgs+=("${SHELL,,}")
  case $DE_WM in
  None);;
  Plasma) pkgs+=("plasma" "konsole");;
  Mate) pkgs+=("mate" "mate-terminal");;
  *) pkgs+=("${DE_WM,,}");;
  esac
  case $LOGIN_MANAGER in
  None);;
  LightDM) pkgs+=("lightdm" "lightdm-gtk-greeter");;
  *) pkgs+=("${LOGIN_MANAGER,,}");;
  esac
  pacstrap -K /mnt "${pkgs[@]}" networkmanager grub $( if [ -f /sys/firmware/efi/fw_platform_size ]; then echo -n "efibootmgr"; fi )
  case $AUR in
  None);;
  Yay)
  pacman -S --noconfirm git
  pacstrap -K /mnt base-devel git debugedit fakeroot go
  if [ -d /mnt/var/tmp/yay ]; then
    rm -r /mnt/var/tmp/yay
  fi
  mkdir /mnt/var/tmp/yay
  arch-chroot /mnt /bin/git clone http://aur.archlinux.org/yay.git /var/tmp/yay
  chown -R nobody:nobody /mnt/var/tmp/yay
  chmod -R 777 /mnt/var/tmp/yay
  cd /mnt/var/tmp/yay
  arch-chroot /mnt /bin/bash -c "su - nobody -s /bin/bash -c \"cd /var/tmp/yay; export HOME=/var/tmp/yay; makepkg\""
  for i in *.tar.zst; do
    [ -f "$i" ] || break
    arch-chroot /mnt /bin/pacman -U --noconfirm /var/tmp/yay/$i
  done
  rm -r /mnt/var/tmp/yay
  cd ~
  ;;
  Paru)
  pacman -S --noconfirm git
  pacstrap -K /mnt base-devel git debugedit fakeroot cargo
  if [ -d /mnt/var/tmp/paru ]; then
    rm -r /mnt/var/tmp/paru
  fi
  mkdir /mnt/var/tmp/paru
  arch-chroot /mnt /bin/git clone http://aur.archlinux.org/paru.git /var/tmp/paru
  chown -R nobody:nobody /mnt/var/tmp/paru
  chmod -R 777 /mnt/var/tmp/paru
  cd /mnt/var/tmp/paru
  arch-chroot /mnt /bin/bash -c "su - nobody -s /bin/bash -c \"cd /var/tmp/paru; export HOME=/var/tmp/paru; makepkg\""
  for i in *.tar.zst; do
    [ -f "$i" ] || break
    arch-chroot /mnt /bin/pacman -U --noconfirm /var/tmp/paru/$i
  done
  rm -r /mnt/var/tmp/paru
  cd ~
  ;;
  esac
  echo "$HOSTNAME" > /mnt/etc/hostname
  echo "$ROOT_PASS" | passwd --root /mnt --stdin
  echo "%wheel ALL=(ALL:ALL) ALL" > /mnt/etc/sudoers.d/wheel
  for user in "${USERS[@]}"; do
    username=$(echo -n "$user" | cut -d":" -f1)
    passwd=$(echo -n "$user" | cut -d":" -f2)
    groups=$(echo -n "$user" | cut -d":" -f3)
    echo "Creating user ($username) with groups ($groups))"
    useradd -R /mnt -m -G "$groups" -s /bin/"${SHELL,,}" "$username"
    echo "$passwd" | passwd --root /mnt --stdin "$username"
  done
  systemctl --root /mnt enable NetworkManager.service
  case $LOGIN_MANAGER in
  None);;
  LightDM)
  systemctl --root /mnt enable "lightdm.service"
  sed -i "s/#greeter-session=.*/greeter-session=lightdm-gtk-greeter/g" /mnt/etc/lightdm/lightdm.conf
  ;;
  *) systemctl --root /mnt enable "${LOGIN_MANAGER,,}.service";;
  esac
  sed -i "s/#${LOCALE}/${LOCALE}/g" /mnt/etc/locale.gen
  arch-chroot /mnt /bin/locale-gen
  touch /mnt/etc/locale.conf
  echo "LANG=$(echo -n "$LOCALE" | cut -d" " -f1 )" > /mnt/etc/locale.conf
  ln -sf /usr/share/zoneinfo/"$TIMEZONE" /mnt/etc/localtime
  arch-chroot /mnt /bin/hwclock --systohc
  genfstab -U /mnt >> /mnt/etc/fstab
  if [ -f /sys/firmware/efi/fw_platform_size ]; then
    if [ -d /mnt/boot/efi ]; then
      arch-chroot /mnt /bin/grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
      arch-chroot /mnt /bin/grub-mkconfig -o /boot/grub/grub.cfg
    else
      echo "[ERROR] (EFI) Could not find device to install grub to!"
    fi
  else
    root="$(findmnt -n /mnt/ | cut -d" " -f2)"
    if [ "$(echo "$root" | xargs)" != "" ]; then
      arch-chroot /mnt /bin/grub-install --target=i386-pc "$root"
      arch-chroot /mnt /bin/grub-mkconfig -o /boot/grub/grub.cfg
    else
      echo "[ERROR] (MBR) Could not find device to install grub to!"
    fi
  fi
  echo "Installation complete!"
  echo "You are now chrooting into your new system"
  echo "From here you are able to install any extra packages you wish or make edit some configuration files"
  echo "When you are done reboot type 'exit' and 'reboot' to do a full boot into your new Arch Linux system"
  arch-chroot /mnt /bin/${SHELL,,}
}

usage() {
  echo "------ ArchUP Usage ------"
  echo "> archup -c <file> | reads an archup config file from the specified location"
  echo "> archup -h/-u | Prints out the usage information"
}

if [ $UID -ne 0 ]; then
  echo "This script must be ran as root"
  exit 1
fi

if ! pacman -Qq | grep -q "^dialog"; then
  pacman -Sy --noconfirm dialog
fi

while getopts "hc:" arg; do
  case $arg in
    c)
      if [ ! -f "${OPTARG}" ]; then
        echo "${OPTARG} is not a valid config file"
        exit 1
      fi
      if ! source "${OPTARG}"; then
        echo "Could not source ${OPTARG}"
        exit 1
      fi
      ;;
    h|u)
      usage
      exit 0
      ;;
    *)
      usage
      exit 1
      ;;
  esac
done

main_menu
