
# ArchUP
## An easy to use Arch Linux installer
##### ArchUP is in no way affiliated with archinstaller, its developers or the arch linux community. It's maintained seperately from people who just like Arch Linux
### Developers:
- [CapCreeperGR ](https://gitlab.com/CapCreeperGR)

### Project Information

##### Warning: This project is still in early alpha. Several systems are still buggy or even unfinished
ArchUP is a project that aims to make a powerful, easy to use and easy to understand Arch Linux installer

### How to use
- Copy the following into your terminal
```
bash <(curl -s https://gitlab.com/CapCreeperGR/archup/-/raw/master/archup.sh)
```
- Follow the steps on screen to install Arch Linux using ArchUP
### Known Issues
- The Partition section is not finished. Partitioning has to be done manually outside of the script at the moment
- Paru compilation can fail due to low memory on some systems
